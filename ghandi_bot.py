
import time
import praw

u = praw.Reddit('Gandhi Bot')
u.login()


misspelled = ['ghandi', 'ghandhi', 'gandi', 'Ghandi', 'Ghandhi', 'Gandi' ]

text = 'You misspelled Gandhi. This is a common mistake.  ' + '\n' + \
        'I am [Gandhi Bot](http://github.com/isthisnagee/Gandhi-Bot). I am always watching'

gandhi_d = []        # keeps id's of comments replied to.


while True:
    all_comments = u.get_comments('all', limit=30)        # gets 30 comments from reddit.com/r/all/comments
    for comment in all_comments:
        #check every word in misspelled to see if it appears in the comment
        has_misspelled = any(word in str(comment) for word in misspelled)  
        if has_misspelled and comment.id not in gandhi_d:
            comment.reply(text)
    time.sleep(30)
    # this is so we dont go over reddit's request limit and get an error, exiting the bot. Unfortunately, the tradeoff is that
    # I can only get 60 comments a minute. It seems good, but when you look at how many comments are posted to reddit every second
    #...lol
